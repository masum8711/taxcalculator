package com.cloud.sologame.currencyconverter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface RetrofitInterface {
        @GET()
        Call<CurrencyModelClass>getCurrency(@Url String stringUrl);
        //Call<ArrayList<CurrencyModelClass>> getCurrencyModelClassCall(@Url String stringUrl);
}
