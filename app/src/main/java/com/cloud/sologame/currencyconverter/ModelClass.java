package com.cloud.sologame.currencyconverter;

public class ModelClass {
    private String countryname;

    public ModelClass(String countryname) {
        this.countryname = countryname;
    }

    public ModelClass() {
    }

    public String getCountryname() {
        return countryname;
    }

    public void setCountryname(String countryname) {
        this.countryname = countryname;
    }
}
