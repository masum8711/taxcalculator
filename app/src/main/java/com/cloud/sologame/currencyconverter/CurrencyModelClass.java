package com.cloud.sologame.currencyconverter;


import android.provider.Telephony;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CurrencyModelClass {

    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("version")
    @Expose
    private Object version;
    @SerializedName("rates")
    @Expose
    private List<Rate> rates = null;

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Object getVersion() {
        return version;
    }

    public void setVersion(Object version) {
        this.version = version;
    }

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> rates) {
        this.rates = rates;
    }

    public class Period {

        @SerializedName("effective_from")
        @Expose
        private String effectiveFrom;
        @SerializedName("rates")
        @Expose
        private Rates rates;

        public String getEffectiveFrom() {
            return effectiveFrom;
        }

        public void setEffectiveFrom(String effectiveFrom) {
            this.effectiveFrom = effectiveFrom;
        }

        public Rates getRates() {
            return rates;
        }

        public void setRates(Rates rates) {
            this.rates = rates;
        }

    }

    public class Rate {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("country_code")
        @Expose
        private String countryCode;
        @SerializedName("periods")
        @Expose
        private List<Period> periods = null;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public List<Period> getPeriods() {
            return periods;
        }

        public void setPeriods(List<Period> periods) {
            this.periods = periods;
        }

    }

    public class Rates {

        @SerializedName("super_reduced")
        @Expose
        private Double superReduced;
        @SerializedName("reduced")
        @Expose
        private Double reduced;
        @SerializedName("standard")
        @Expose
        private Double standard;
        @SerializedName("reduced1")
        @Expose
        private Double reduced1;
        @SerializedName("reduced2")
        @Expose
        private Double reduced2;
        @SerializedName("parking")
        @Expose
        private Double parking;

        public Double getSuperReduced() {
            return superReduced;
        }

        public void setSuperReduced(Double superReduced) {
            this.superReduced = superReduced;
        }

        public Double getReduced() {
            return reduced;
        }

        public void setReduced(Double reduced) {
            this.reduced = reduced;
        }

        public Double getStandard() {
            return standard;
        }

        public void setStandard(Double standard) {
            this.standard = standard;
        }

        public Double getReduced1() {
            return reduced1;
        }

        public void setReduced1(Double reduced1) {
            this.reduced1 = reduced1;
        }

        public Double getReduced2() {
            return reduced2;
        }

        public void setReduced2(Double reduced2) {
            this.reduced2 = reduced2;
        }

        public Double getParking() {
            return parking;
        }

        public void setParking(Double parking) {
            this.parking = parking;
        }

    }
}