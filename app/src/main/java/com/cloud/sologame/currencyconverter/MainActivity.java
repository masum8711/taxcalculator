package com.cloud.sologame.currencyconverter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.HEAD;

import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AdapterView;

import static java.lang.String.valueOf;


public class MainActivity extends AppCompatActivity implements OnItemSelectedListener, View.OnClickListener {

    private static final String BASE_URL = "https://jsonvat.com/";

    private String TAG = "Retrofit";
    private Retrofit retrofit;
    private RetrofitInterface retrofitInterface;


    private RadioGroup currencyStatus;
    private EditText inputAmountET;

    private Spinner mSpinner;
    private ArrayList<String> spinnerArray = new ArrayList<String>();
    private ArrayList<CurrencyModelClass.Rate> ratesList = new ArrayList<>();

    private RadioButton radioButton_1;
    private RadioButton radioButton_2;
    private RadioButton radioButton_3;
    private RadioButton radioButton_4;
    private RadioButton radioButton_5;
    private TextView taxView;
    private Button calculateButton , reloadButton;

    private CurrencyModelClass currencyModelClass;
    private CurrencyModelClass.Rates selectedRates;
    private double selectedTaxMethod = -1.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //urlString = String.format("0/0");
        currencyStatus = findViewById(R.id.currentStatusRB);
        inputAmountET = findViewById(R.id.inputET);
        radioButton_1 = findViewById(R.id.radioButton_1);
        radioButton_2 = findViewById(R.id.radioButton_2);
        radioButton_3 = findViewById(R.id.radioButton_3);
        radioButton_4 = findViewById(R.id.radioButton_4);
        radioButton_5 = findViewById(R.id.radioButton_5);
        taxView = (TextView)findViewById(R.id.taxID);
        calculateButton = (Button)findViewById(R.id.calculateTaxID);
        reloadButton = (Button)findViewById(R.id.reloadButtonID);
        calculateButton.setOnClickListener(this);
        reloadButton.setOnClickListener(this);

        mSpinner = findViewById(R.id.spinner);
        mSpinner.setOnItemSelectedListener(this);
        /*Radio Group*/
        radioMathod();

        /*JSON PARSING*/
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofitInterface = retrofit.create(RetrofitInterface.class);
        sendRequestToServer();
    }

    private void setUpRadioButtonWith(CurrencyModelClass.Rate rate){

        radioButton_1.setVisibility(View.GONE);
        radioButton_2.setVisibility(View.GONE);
        radioButton_3.setVisibility(View.GONE);
        radioButton_4.setVisibility(View.GONE);
        radioButton_5.setVisibility(View.GONE);

        CurrencyModelClass.Rates rates = rate.getPeriods().get(0).getRates();
        selectedRates = rates;

        if(rates.getStandard() != null && rates.getStandard() > 0.0){
            radioButton_1.setVisibility(View.VISIBLE);
            radioButton_1.setText("Standard: "+rates.getStandard());
            radioButton_1.setChecked(true);
            selectedTaxMethod = rates.getStandard();
        }

        if(rates.getParking() != null && rates.getParking() > 0.0){
            radioButton_2.setVisibility(View.VISIBLE);
            radioButton_2.setText("parking: "+rates.getParking());
        }

        if(rates.getReduced1() != null && rates.getReduced1() > 0.0){
            radioButton_3.setVisibility(View.VISIBLE);
            radioButton_3.setText("Reduced1: "+rates.getReduced1());
        }

        if(rates.getReduced2() != null && rates.getStandard() > 0.0){
            radioButton_4.setVisibility(View.VISIBLE);
            radioButton_4.setText("Reduced2: "+rates.getReduced2());
        }

        if(rates.getSuperReduced() != null && rates.getStandard() > 0.0){
            radioButton_5.setVisibility(View.VISIBLE);
            radioButton_5.setText("Super Reduced: "+ rates.getSuperReduced());
        }
    }

    private void radioMathod() {
        currencyStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                switch (id){
                    case -1:
                        Log.d(TAG, "Choices cleared!!");
                        break;
                    case R.id.radioButton_1:
                        selectedTaxMethod = selectedRates.getStandard();
                        break;
                    case R.id.radioButton_2:
                        selectedTaxMethod = selectedRates.getParking();
                        break;
                    case R.id.radioButton_3:
                        selectedTaxMethod = selectedRates.getReduced1();
                        break;
                    case R.id.radioButton_4:
                        selectedTaxMethod = selectedRates.getReduced2();
                        break;
                    case R.id.radioButton_5:
                        selectedTaxMethod = selectedRates.getSuperReduced();
                        break;
                }

                calculateButton.performClick();
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        CurrencyModelClass.Rate rate = currencyModelClass.getRates().get(position);
        selectedTaxMethod = -1.0;
        taxView.setText("Press Calculate Button");
        setUpRadioButtonWith(rate);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void calculateAndSetValue(double tax){

        taxView.setText("Capital : "+inputAmountET.getText().toString().trim()+ "\n"
                        +"Calculated Tax : " + tax +"\n"
                        +"Subtotal : "+ (Double.parseDouble(inputAmountET.getText().toString().trim()) + tax));
    }

    @Override
    public void onClick(View v) {
        switch (Integer.parseInt(v.getTag().toString())) {
            case 1:
                sendRequestToServer();
                break;
            case  2: {
                if (inputAmountET.getText().toString().trim().length() == 0)
                    Toast.makeText(MainActivity.this, "Please Input Ammount First!!!!", 1).show();
                else {
                    Double capital = Double.parseDouble(inputAmountET.getText().toString());
                    Double totalTax = (capital * selectedTaxMethod) / 100.0;
                    calculateAndSetValue(totalTax);
                }
            }
        }
    }


    public static boolean isInternetAvailable(Context context)
    {
        NetworkInfo info = (NetworkInfo) ((ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        if (info == null)
        {
            Log.d("Main","no internet connection");
            return false;
        }
        else
        {
            if(info.isConnected())
            {
                Log.d("Main"," internet connection available...");
                return true;
            }
            else
            {
                Log.d("Main"," internet connection");
                return true;
            }

        }
    }

    public void sendRequestToServer(){
        Call<CurrencyModelClass> getCurrencyModelClassCall = retrofitInterface.getCurrency("0/0");

        getCurrencyModelClassCall.enqueue(new Callback<CurrencyModelClass>() {
            @Override
            public void onResponse(Call<CurrencyModelClass> call, Response<CurrencyModelClass> response) {
                reloadButton.setVisibility(View.GONE);
                currencyModelClass = response.body();
                List<CurrencyModelClass.Rate> currencyRate = currencyModelClass.getRates();

                ArrayList<String> name = new ArrayList<String>();
                for (CurrencyModelClass.Rate rate : currencyRate) {
                    String countryName = rate.getName();
                    spinnerArray.add(countryName);
                }

                ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(getApplicationContext(),
                        android.R.layout.simple_spinner_item,
                        spinnerArray);

                mSpinner.setAdapter(spinnerArrayAdapter);
                mSpinner.setSelection(0);
            }

            @Override
            public void onFailure(Call<CurrencyModelClass> call, Throwable t) {
                reloadButton.setVisibility(View.VISIBLE);
                Toast.makeText(MainActivity.this, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();

            }
        });
    }

}
